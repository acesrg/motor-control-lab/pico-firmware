#include <lib/driver/pwm.h>

float mock_setted_duty, mock_delayed_ms;

static retval_t mock_pwm_init(driver_pwm_t *driver) {
    return RV_SUCCESS;
}

static retval_t mock_set_duty(driver_pwm_t *driver, float duty) {
    mock_setted_duty = duty;
    return RV_SUCCESS;
}

void mock_delay_ms(float ms) {
    mock_delayed_ms = ms;
}

driver_pwm_t pwm_mock = {
    .config = {
        .driver_pwm_out = 0,
        .driver_pwm_channel = 0,
        .driver_pwm_frequency = 50,
    },
    .init = mock_pwm_init,
    .set_duty = mock_set_duty,
};
