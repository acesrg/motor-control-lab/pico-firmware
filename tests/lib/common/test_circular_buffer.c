#include <lib/common/circular_buffer.h>
#include "unity_fixture.h"

TEST_GROUP(circular_buffer);

TEST_SETUP(circular_buffer) {}
TEST_TEAR_DOWN(circular_buffer) {}

TEST(circular_buffer, test_definition)
{
    DEFINE_CIRCULAR_BUFFER(cbuf, 4);
    TEST_ASSERT_EQUAL(cbuf.size, 4);
    TEST_ASSERT_EQUAL(cbuf.write_pos, 0);
    TEST_ASSERT_EQUAL(cbuf.read_pos, 0);
}

TEST(circular_buffer, test_write)
{
    frame_t frame_single = DECLARE_FRAME_SPACE(1);
    frame_single.buf[0] = 'a';

    DEFINE_CIRCULAR_BUFFER(cbuf, 6);
    size_t write_pos_previous = cbuf.write_pos;

    retval_t rv;
    rv = circular_buffer_write_frame(&cbuf, &frame_single);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(write_pos_previous + frame_single.size, cbuf.write_pos);
    TEST_ASSERT_EQUAL_UINT8(cbuf.buf[write_pos_previous], 'a');

    write_pos_previous = cbuf.write_pos;
    frame_t frame_five = DECLARE_FRAME_SPACE(5);
    frame_five.buf[0] = 'b';
    frame_five.buf[1] = 'c';
    frame_five.buf[2] = 'd';
    frame_five.buf[3] = 'e';
    frame_five.buf[4] = 'f';

    rv = circular_buffer_write_frame(&cbuf, &frame_five);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(true, cbuf.full);
    TEST_ASSERT_EQUAL(0, cbuf.write_pos);  // should go back to 0
    TEST_ASSERT_EQUAL_CHAR_ARRAY("bcdef", cbuf.buf + frame_single.size, 5);

    rv = circular_buffer_write_frame(&cbuf, &frame_five);
    TEST_ASSERT_EQUAL(RV_NOSPACE, rv);  // no more space, since no read
}

TEST(circular_buffer, test_read)
{
    // first write a frame
    frame_t frame_write = DECLARE_FRAME_SPACE(3);
    frame_write.buf[0] = '1';
    frame_write.buf[1] = '2';
    frame_write.buf[2] = '3';

    DEFINE_CIRCULAR_BUFFER(cbuf, 3);

    retval_t rv;
    rv = circular_buffer_write_frame(&cbuf, &frame_write);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(true, cbuf.full);

    frame_t frame_read = DECLARE_FRAME_SPACE(1);
    rv = circular_buffer_read_frame(&cbuf, &frame_read);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(false, cbuf.full);
    TEST_ASSERT_EQUAL_CHAR_ARRAY("1", frame_read.buf, 1);

    frame_t frame_read2 = DECLARE_FRAME_SPACE(2);
    rv = circular_buffer_read_frame(&cbuf, &frame_read2);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(false, cbuf.full);
    TEST_ASSERT_EQUAL_CHAR_ARRAY("23", frame_read2.buf, 2);

    size_t read_pos_prev = cbuf.read_pos;
    rv = circular_buffer_read_frame(&cbuf, &frame_read2);
    TEST_ASSERT_EQUAL(RV_NOSPACE, rv);
    TEST_ASSERT_EQUAL(read_pos_prev, cbuf.read_pos);
}


TEST(circular_buffer, test_multi_read_writes)
{
    // declare a couple frames
    frame_t frame_foo = DECLARE_FRAME_SPACE(6);
    frame_foo.buf = "..foo";
    frame_t frame_ba = DECLARE_FRAME_SPACE(4);
    frame_ba.buf = "..ba";

    frame_t reading_1 = DECLARE_FRAME_SPACE(1);
    frame_t reading_5 = DECLARE_FRAME_SPACE(5);

    DEFINE_CIRCULAR_BUFFER(cbuf, 7);

    retval_t rv;
    rv = circular_buffer_write_frame(&cbuf, &frame_foo);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_5);
    TEST_ASSERT_EQUAL(RV_NOSPACE, rv);  // nospace!
    rv = circular_buffer_write_frame(&cbuf, &frame_ba);
    TEST_ASSERT_EQUAL(RV_NOSPACE, rv);  // nospace!
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_write_frame(&cbuf, &frame_ba);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_5);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    rv = circular_buffer_read_frame(&cbuf, &reading_1);
    TEST_ASSERT_EQUAL(RV_NOSPACE, rv); // nospace!
}

TEST_GROUP_RUNNER(circular_buffer) {
    RUN_TEST_CASE(circular_buffer, test_definition);
    RUN_TEST_CASE(circular_buffer, test_write);
    RUN_TEST_CASE(circular_buffer, test_read);
    RUN_TEST_CASE(circular_buffer, test_multi_read_writes);
}
