#include "unity_fixture.h"

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST_GROUP(dummy);
    RUN_TEST_GROUP(circular_buffer);
    return UNITY_END();
}
