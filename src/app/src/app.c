/* Copyright 2023 ACES-RG */
#include <stdio.h>

#include <pico/stdlib.h>

#include <FreeRTOS.h>
#include <task.h>

void dummy_task(void *pvParameters) {
    stdio_init_all();
    for (;;) {
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

int main() {
    xTaskCreate(&dummy_task, "Dummy", 256, NULL, 2, NULL);
    vTaskStartScheduler();
    while (1) {
        // hi
    }
}
