/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_COMMON_CIRCULAR_BUFFER_H_
#define SRC_LIB_COMMON_CIRCULAR_BUFFER_H_
#include <stddef.h>
#include <stdbool.h>

#include <lib/common/retval.h>
#include <lib/common/frame.h>

typedef struct {
    uint8_t *buf;
    size_t size;
    size_t read_pos;
    size_t write_pos;
    bool full;
} circular_buffer_t;

retval_t circular_buffer_read_frame(circular_buffer_t *buf, frame_t *frame);
retval_t circular_buffer_write_frame(circular_buffer_t *buf, frame_t *frame);

#define DECLARE_CIRCULAR_BUFFER(__buf, __size) {    \
    .buf = (uint8_t *)(__buf),                      \
    .size = (__size),                               \
    .read_pos = 0,                                  \
    .write_pos = 0,                                 \
    .full = false,                                  \
}

#define DECLARE_CIRCULAR_BUFFER_SPACE(__buf_size) DECLARE_CIRCULAR_BUFFER((uint8_t[__buf_size]){}, __buf_size)

#define DEFINE_CIRCULAR_BUFFER(_name, _size)  \
    circular_buffer_t _name = DECLARE_CIRCULAR_BUFFER_SPACE(_size)

#endif  // SRC_LIB_COMMON_CIRCULAR_BUFFER_H_
