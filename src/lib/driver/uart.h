/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_DRIVER_UART_H_
#define SRC_LIB_DRIVER_UART_H_

#include <stdint.h>
#include <stddef.h>
#include <lib/common/retval.h>
#include <lib/common/frame.h>
#include <lib/common/circular_buffer.h>


typedef struct {
    uint8_t id;
    uint32_t baud_rate;
    uint8_t data_bits;
    uint8_t stop_bits;
    uint8_t parity;
    uint8_t tx_pin;
    uint8_t rx_pin;
    void *instance;
} uart_config_t;

typedef struct uart_s uart_t;

typedef retval_t (uart_send_t)(uart_t *uart, frame_t *send_frame);
typedef retval_t (uart_recv_t)(uart_t *uart, frame_t *recv_frame);
typedef retval_t (uart_init_t)(uart_t *uart);

struct uart_s {
    uart_config_t config;
    circular_buffer_t *cbuf;
    uart_send_t *send;
    uart_recv_t *recv;
    uart_init_t *init;
};

retval_t uart_dev_init(uart_t *uart);
retval_t uart_send(uart_t *uart, frame_t *send_frame);
retval_t uart_recv(uart_t *uart, frame_t *recv_frame);
retval_t uart_recv_from_isr(uart_t *uart, frame_t *recv_frame);

#define DECLARE_UART_CONFIG(__id, __baud_rate, __data_bits, __stop_bits, __parity, __tx_pin, __rx_pin)  \
    {                               \
        .id = __id,                 \
        .baud_rate = __baud_rate,   \
        .stop_bits = __stop_bits,   \
        .data_bits = __data_bits,   \
        .parity = __parity,         \
        .tx_pin = __tx_pin,         \
        .rx_pin = __rx_pin,         \
    }

#define DEFINE_UART(_name, _id, _baud_rate, _data_bits, _stop_bits, _parity, _tx_pin, _rx_pin, _cbuf)               \
    uart_t _name = {                                                                                                \
        .config = DECLARE_UART_CONFIG(_id, _baud_rate, _data_bits, _stop_bits, _parity, _tx_pin, _rx_pin),          \
        .cbuf = _cbuf,              \
        .send = uart_send,          \
        .recv = uart_recv,          \
        .init = uart_dev_init,      \
    }

#endif  // SRC_LIB_DRIVER_UART_H_
