/* Copyright 2023 ACES-RG */
#include <stdio.h>

#include <lib/driver/uart.h>
#include <lib/common/circular_buffer.h>
#include "hardware/irq.h"
#include "pico/stdlib.h"

static uart_t *_uart0 = NULL;
static uart_t *_uart1 = NULL;

static frame_t isr_frame = DECLARE_FRAME_SPACE(1);

void uart0_isr() {
    if (NULL == _uart0) {
        printf("UART INIT ERROR\n");
        return;
    }
    isr_frame.buf[0] = uart_getc(uart0);
    circular_buffer_write_frame(_uart0->cbuf, &isr_frame);
}

void uart1_isr() {
    if (NULL == _uart1) {
        printf("UART INIT ERROR\n");
        return;
    }
    isr_frame.buf[0] = uart_getc(uart1);
    circular_buffer_write_frame(_uart1->cbuf, &isr_frame);
}

retval_t uart_dev_init(uart_t *uart) {
    if (NULL == uart) {
        return RV_ILLEGAL;
    }
    uart->config.instance = (uart_inst_t *) uart_get_instance(uart->config.id);
    uart_inst_t *instance = uart_get_instance(uart->config.id);

    uart_init(uart->config.instance, uart->config.baud_rate);

    gpio_set_function(uart->config.tx_pin, GPIO_FUNC_UART);
    gpio_set_function(uart->config.rx_pin, GPIO_FUNC_UART);

    uart_set_hw_flow(uart->config.instance, false, false);

    uart_set_format(uart->config.instance, uart->config.data_bits, uart->config.stop_bits, uart->config.parity);

    irq_set_exclusive_handler(UART0_IRQ, &uart0_isr);
    irq_set_exclusive_handler(UART1_IRQ, &uart1_isr);
    int UART_IRQ = uart->config.id == 0 ? UART0_IRQ : UART1_IRQ;
    irq_set_enabled(UART_IRQ, true);
    uart_set_irq_enables(uart->config.instance, true, false);

    switch (uart->config.id) {
        case 0:
            _uart0 = uart;
            break;
        case 1:
            _uart1 = uart;
            break;
        default:
            return RV_ERROR;
    }
    return RV_SUCCESS;
}

retval_t uart_send(uart_t *uart, frame_t *send_frame) {
    return RV_NOTIMPLEMENTED;
}

retval_t uart_recv(uart_t *uart, frame_t *recv_frame) {
    if (uart_is_readable(uart->config.instance)) {
        uart_read_blocking(uart->config.instance, recv_frame->buf, recv_frame->size);
    }
}

static retval_t uart_flush(uart_inst_t *uart) {
    while (uart_is_readable(uart)) {
        uart_getc(uart);
    }
}
