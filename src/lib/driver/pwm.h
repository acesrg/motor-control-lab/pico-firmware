/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_DRIVER_PWM_H_
#define SRC_LIB_DRIVER_PWM_H_
/* local libs */
#include <stdint.h>
#include <lib/common/retval.h>

typedef struct {
    uint8_t driver_pwm_out;
    uint8_t driver_pwm_channel;
    uint16_t driver_pwm_frequency;
} driver_pwm_config_t;

typedef struct driver_pwm_s driver_pwm_t;

typedef retval_t (driver_pwm_init_f)(driver_pwm_t *driver);
typedef retval_t (driver_pwm_set_f)(driver_pwm_t *driver, float duty);

struct driver_pwm_s {
    float duty;
    driver_pwm_config_t config;
    driver_pwm_init_f *init;
    driver_pwm_set_f *set_duty;
};

retval_t driver_pwm_init(driver_pwm_t *driver);
retval_t driver_pwm_set_duty(driver_pwm_t *driver, float duty);

#define DECLARE_PWM_CONFIG(__driver_pwm_out, __driver_pwm_channel, __driver_pwm_frequency)  \
    {                                                      \
        .driver_pwm_out = __driver_pwm_out,                \
        .driver_pwm_channel = __driver_pwm_channel,        \
        .driver_pwm_frequency = __driver_pwm_frequency,    \
    }

#define DEFINE_PWM(_name, _driver_pwm_out, _driver_pwm_channel, _driver_pwm_frequency)              \
    driver_pwm_t _name = {                                                                          \
        .config = DECLARE_PWM_CONFIG(_driver_pwm_out, _driver_pwm_channel, _driver_pwm_frequency),  \
        .init = driver_pwm_init,                                                                    \
        .set_duty = driver_pwm_set_duty,                                                            \
    }

#endif  // SRC_LIB_DRIVER_PWM_H_
