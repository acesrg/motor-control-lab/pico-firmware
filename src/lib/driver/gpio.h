/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_DRIVER_GPIO_H_
#define SRC_LIB_DRIVER_GPIO_H_

#include <stdint.h>
#include <lib/common/retval.h>

typedef enum {
    GPIO_DIR_INPUT,
    GPIO_DIR_OUTPUT,
} gpio_dir_t;

typedef enum {
    GPIO_STATE_OFF,
    GPIO_STATE_ON,
    GPIO_STATE_UNDEFINED,
} gpio_state_t;

typedef struct {
    uint8_t gpio;
    gpio_dir_t dir;
    gpio_state_t init_state;
} gpio_config_t;

typedef struct gpio_s gpio_t;

typedef retval_t (gpio_init_f)(gpio_t *gpio);
typedef retval_t (gpio_power_f)(gpio_t *gpio);
typedef gpio_state_t (gpio_get_f)(gpio_t *gpio);

struct gpio_s {
    gpio_config_t *config;
    gpio_state_t _state_cached;
    gpio_init_f *init;
    gpio_power_f *on;
    gpio_power_f *off;
    gpio_power_f *toggle;
    gpio_get_f *get;
};

retval_t gpio_driver_init(gpio_t *gpio);
retval_t gpio_driver_on(gpio_t *gpio);
retval_t gpio_driver_off(gpio_t *gpio);
retval_t gpio_driver_toggle(gpio_t *gpio);
gpio_state_t gpio_driver_get(gpio_t *gpio);

#define DEFINE_GPIO(_name, _config)         \
    gpio_t _name = {                        \
        .config = _config,                  \
        ._state_cached = GPIO_STATE_OFF,    \
        .init = gpio_driver_init,           \
        .on = gpio_driver_on,               \
        .off = gpio_driver_off,             \
        .toggle = gpio_driver_toggle,       \
        .get = gpio_driver_get,             \
    }
#endif  // SRC_LIB_DRIVER_GPIO_H_
